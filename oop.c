/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stddef.h>

#include "oop.h"

OOP_CLASS_DEFINE(oop_class_class_impl, );

struct oop_class const *oop_class_class = (void const*)&oop_class_class_impl;

struct oop_class_impl_0 {
	struct oop_object_header oop_header;
	uint64_t oop_version;
	size_t oop_size;
	struct oop_class_dict_entry oop_pairs[];
};

void (*oop_class_lookup_method(struct oop_class_inline_cache *cache,
			       struct oop_class const *class, uint64_t low,
			       uint64_t high))(void)
{
	for (size_t ii = 0U; ii < OOP_ARRAY_SIZE(cache->__pairs); ++ii) {
		struct oop_class const *maybe_class;
		void (*maybe_method)(void);

		maybe_class = cache->__pairs[ii].__class;
		maybe_method = cache->__pairs[ii].__method;

		if (maybe_class == class)
			return maybe_method;
	}

	switch (class->oop_version) {
	case OOP_CLASS_ABI_VERSION_0: {
		struct oop_class_impl_0 const *class_0 = (void const*)class;

		void (*method_impl)(void);
		size_t size = class_0->oop_size;
		struct oop_class_dict_entry const *pairs = class_0->oop_pairs;
		for (size_t ii = 0U; ii < size; ++ii) {
			struct oop_class_dict_entry const *pair = &pairs[ii];
			uint64_t maybe_low = pair->oop_low;
			uint64_t maybe_high = pair->oop_high;
			void (*maybe_method)(void) = pair->oop_method;

			if (low == maybe_low && high == maybe_high) {
				method_impl = maybe_method;
				goto got_method;
			}
		}

		return 0;

	got_method:
		;
		unsigned char counter = cache->__counter;
		cache->__counter = (counter + 1U) % OOP_ARRAY_SIZE(cache->__pairs);

		cache->__pairs[counter].__class = class;
		cache->__pairs[counter].__method = method_impl;

		return method_impl;
	}

	default:
		__builtin_unreachable();
	}
}
